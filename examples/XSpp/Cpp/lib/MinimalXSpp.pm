package MinimalXSpp;
use 5.006001;
use strict;
use warnings;
use DateTime;

our $VERSION = '0.01';

require XSLoader;
XSLoader::load('MinimalXSpp', $VERSION);

1;
__END__

=head1 NAME


=head1 SYNOPSIS


=head1 DESCRIPTION


=head1 AUTHOR


=head1 COPYRIGHT AND LICENSE

This library is free software; you can redistribute it and/or modify
it under the same terms as Perl itself, either Perl version 5.6.1 or,
at your option, any later version of Perl 5 you may have available.

=cut

