#!/usr/bin/perl -w
use strict;
use Module::Build::WithXSpp;

my $build = Module::Build::WithXSpp->new(
  module_name     => 'MinimalXSpp',
  license         => 'perl',
  'build_requires' => {
    'ExtUtils::Typemaps::Default' => '1.05',
    'ExtUtils::XSpp' => '0.18',
    'Module::Build' => '0.4211',
    'Test::More' => '0'
  },
  'configure_requires' => {
    'ExtUtils::CppGuess' => '0.07',
    'Module::Build' => '0.4211',
    'Module::Build::WithXSpp' => '0.13'
  },
  extra_compiler_flags => [qw(-std=c++11 -Iinclude_wrapper -Wno-write-strings -Wno-literal-suffix)],
  # Provides useful extra C typemaps for opaque objects:
  extra_typemap_modules => {
    'ExtUtils::Typemaps::Default' => '1.05',
  },
  extra_xs_dirs => [qw(xsp xspt)]
);

$build->create_build_script;
