#!/usr/bin/perl 

use FFI::Platypus;

my $ffi = FFI::Platypus->new;
$ffi->lang('CPP');
$ffi->lib('./simple.so');

$ffi->attach('GiveMeAString', [] => 'string');

print GiveMeAString();


