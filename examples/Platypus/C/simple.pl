#!/usr/bin/perl 

use strict;
use warnings;

use FFI::Platypus;

my $ffi = FFI::Platypus->new;
$ffi->lib('./simple.so');

$ffi->attach('GiveMeAString', [] => 'string');

print GiveMeAString();

