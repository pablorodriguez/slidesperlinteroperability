#!/usr/bin/perl 

use strict;
use warnings;
use Inline "CPP";

print GiveMeAString();

__END__
__CPP__
#include <string>
std::string GiveMeAStringCPP() {
    return std::string("Perl Rocks!");
}

SV* GiveMeAString() {
    std::string str = GiveMeAStringCPP();
    return newSVpvn(str.c_str(), str.length());
}


