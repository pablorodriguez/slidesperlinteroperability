### Perl Interoperability

#### Madrid.pm 2016-03-31

---

### [Language interoperability](https://en.wikipedia.org/wiki/Language_interoperability)

Wikipedia: "the capability of two different programming languages to natively interact and operate on the same kind of data structures"

---

### C

<p class="fragment">Perl is implemented in C!</p>
<p class="fragment"> So... it should be easy... </p>
<p class="fragment">but you should know the [Perl API, internals...](http://perldoc.perl.org/index-internals.html)</p> 

___

### [Perl XS](http://perldoc.perl.org/perlxs.html)

What is XS? (eXtenSion...)

* interface description format
* a language that provide the glue between Perl code and C code

Less work for us... but you should know XS...

___

### [Perl API](http://perldoc.perl.org/perlapi.html)

The set of functions, macros, flags and variables that may be used by extension writers, including:

* Array manipulation functions
* Hash manipulation functions
* Scalar manipulation functions
* Calling functions
* ...

___

### Small function body using XS and API

```perl
dVAR; dXSARGS;
char * string = GiveMeAString();
ST(0) = newSVpv( string, 0);
sv_2mortal(ST(0));
XSRETURN(1);
```

Only glue code...
___

### YAPH?

Other options:

* [FFI::Platypus](https://metacpan.org/pod/FFI::Platypus)
* [Inline](https:://metacpan.org/pod/Inline)
___

### FFI::Platypus::C

```c
char * GiveMeAString() {
    return "Perl Rocks!";
}
```

```bash
$ gcc --shared -o simple.so -fPIC simple.c 
```

```perl
use FFI::Platypus;

my $ffi = FFI::Platypus->new;
$ffi->lib('./simple.so');

$ffi->attach('GiveMeAString', [] => 'string');

print GiveMeAString();
```
___

### Inline::C

```perl
use Inline "C";

print GiveMeAString();

__END__
__C__
char * GiveMeAString() {
    return "Perl Rocks!";
}

```

---

### C++

Uhmmm... it seems so similar to C...

Are you sure?

* STL
* Templates
* Constexpr
* Metaprogramming
* ... 
___

### Small function body using XS and API

```perl
dVAR; dXSARGS;
std::string str = GiveMeAString();
ST(0) = newSVpvn( str.c_str(), str.length());
sv_2mortal(ST(0));
XSRETURN(1);
```

___

### FFI::Platypus::Cpp

```cpp
#include <string>
std::string * GiveMeAString() {
    return std::string("Perl Rocks!");
}
```

```bash
$ g++ --shared -o simple.so -fPIC simple.c 
```

```perl
use FFI::Platypus;

my $ffi = FFI::Platypus->new;
$ffi->lang('CPP');
$ffi->lib('./simple.so');

$ffi->attach('GiveMeAString', [] => 'string');

print GiveMeAString();
```

Don't work...
___

### Inline::CPP

```perl
use Inline "CPP";

print GiveMeAString();

__END__
__CPP__
#include <string>
std::string GiveMeAString() {
    return std::string("Perl Rocks!");
}
```

But this don't work, we need the typemap (or glue code) for std::string! (As Inline::CPP 0.74)

___

### Inline::CPP

```perl
use Inline "CPP";

print GiveMeAString();

__END__
__CPP__
#include <string>
std::string GiveMeAStringCPP() {
    return std::string("Perl Rocks!");
}
SV* GiveMeAString() {
    std::string str = GiveMeAStringCPP();
    return newSVpvn(str.c_str(), str.length());
}
```
This works!
___

### ExtUtils::XSpp

```
#include <string>

%module{MinimalXSpp};

%loadplugin{feature::default_xs_typemap};

%name{MinimalXSpp::GiveMeAString} std::string GiveMeAString()
    %code{%
        RETVAL = std::string("Perl Rocks!");
    %}; 
```

```perl
use MinimalXSpp;

print MinimalXSpp::GiveMeAString();
```

---

### Java

Via C++ with gcj and FFI::Platypus

Not so easy...

---

### R

[Statistics::NiceR](https://metacpan.org/pod/Statistics::NiceR)

```perl
use Statistics::NiceR;
 
my $r = Statistics::NiceR->new();
 
say $r->pnorm( [ 0 .. 3 ] );
```

---

### (Your favorite language here)

Via C API almost every language

---

### CPAN Modules

#### Multiple languages:

* [FFI::Platypus](https://metacpan.org/pod/FFI::Platypus)
* [Inline](https:://metacpan.org/pod/Inline)

#### Single languages:

* [ExtUtils::XSpp](https://metacpan.org/pod/ExtUtils::XSpp)
* [Statistics::NiceR](https://metacpan.org/pod/Statistics::NiceR)

---

### Where to start?

Try first with [FFI::Platypus](https://metacpan.org/pod/FFI::Platypus)



