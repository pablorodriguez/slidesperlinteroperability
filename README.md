## Perl Interoperability

Slides about Perl interoperability with other languages (mainly C++)

## Usage

```
#!bash
cpanm Carton
carton
carton exec revealup serve slides.md --theme sky.css

```
